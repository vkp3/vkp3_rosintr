rosservice call /turtle1/set_pen 0 255 0 2 1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['-3.0, 0.0, 0.0'] ['0.0, 0.0, 0.0']
rosservice call /turtle1/set_pen 0 255 0 2 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['1.0, -2.0, 0.0'] ['0.0, 0.0, 0.0']
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['1.0, 2.0, 0.0'] ['0.0, 0.0, 0.0']
rosservice call /turtle1/set_pen 255 0 0 2 1
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['1.0, 0.0, 0.0'] ['0.0, 0.0, 0.0']
rosservice call /turtle1/set_pen 255 0 0 2 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['-1.0, -2.0, 0.0'] ['0.0, 0.0, 0.0']
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['0.5, 1.0, 0.0'] ['0.0, 0.0, 0.0']
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['1.0, 0.0, 0.0'] ['0.0, 0.0, 0.0']
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['0.5, -1.0, 0.0'] ['0.0, 0.0, 0.0']
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \ ['-1.0, 2.0, 0.0'] ['0.0, 0.0, 0.0']
