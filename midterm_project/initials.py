#!/usr/bin/env python

# Python 2/3 compatibility imports
from __future__ import print_function
from shutil import move
#from macpath import join
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


def go_to_joint_state(move_group, joint_0, joint_1, joint_2, joint_3, joint_4, joint_5):
    
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = joint_0
    joint_goal[1] = joint_1
    joint_goal[2] = joint_2
    joint_goal[3] = joint_3
    joint_goal[4] = joint_4
    joint_goal[5] = joint_5

    move_group.go(joint_goal, wait=True)

    move_group.stop()


def reset(move_group):
    go_to_joint_state(move_group, 0, -pi/8, pi/4, 0, 0, 0)


def main():
    #initialize moveit_commander and a rospy node
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("midterm_project", anonymous=True)

    #Instantiate a RobotCommander object
    robot = moveit_commander.RobotCommander()

    #Instantiate a MoveGroupCommander object
    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    # reset position to beginning of V
    reset(move_group)
    
    # make V
    # move the first diagonal of a V: [-pi/4, -pi/4, pi/2, 0, 0, 0] as joint angles
    go_to_joint_state(move_group, -pi/4, -pi/4, pi/2, 0, 0, 0)
     # move the second diagonal of a V: [-pi/4, 0, 0, 0, 0, 0] as joint angles
    go_to_joint_state(move_group, -pi/4, 0, 0, 0, 0, 0)

    # reset position to beginning of K
    reset(move_group)

    # make K
    # trace the | of the K: [0, -pi/3, 2*pi/3, 0, 0, 0] as joint angles
    go_to_joint_state(move_group, 0, -pi/3, 2*pi/3, 0, 0, 0)
    # trace half way up the vertical line of K: [0, -pi/4, pi/2, 0, 0, 0] as joint angles
    go_to_joint_state(move_group, 0, -pi/4, pi/2, 0, 0, 0)
    # trace top diagonal of the K:
    go_to_joint_state(move_group, -pi/6, 0, 0, 0, 0, 0)
    # trace back to half way up the vertical line of K:
    go_to_joint_state(move_group, 0, -pi/4, pi/2, 0, 0, 0)
    # trace bottom diagonal of the K:
    go_to_joint_state(move_group, -pi/3, -pi/3, 2*pi/3, 0, 0, 0)

    # reset position to beginning of P
    reset(move_group)

    # trace the vertical of the P:
    go_to_joint_state(move_group, 0, -pi/3, 2*pi/3, 0, 0, 0)
    # trace back up the vertical of P:
    reset(move_group)
    # trace top curve of P:
    go_to_joint_state(move_group, -pi/8, -pi/8, pi/4, 0, 0, 0)
    go_to_joint_state(move_group, -pi/6, -pi/8, pi/4, 0, 0, 0)
    # trace bottom curve of P:
    go_to_joint_state(move_group, -pi/6, -pi/4, pi/2, 0, 0, 0)
    go_to_joint_state(move_group, 0, -pi/4, pi/2, 0, 0, 0)


if __name__ == "__main__":
    main()
